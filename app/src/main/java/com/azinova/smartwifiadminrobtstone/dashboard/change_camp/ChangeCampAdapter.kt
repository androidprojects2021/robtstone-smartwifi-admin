package com.azinova.smartwifiadminrobtstone.dashboard.change_camp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.azinova.model.response.login.Hotspots
import com.azinova.smartwifiadminrobtstone.R
import kotlinx.android.synthetic.main.adapter_hotspot.view.*

class ChangeCampAdapter(
    val context: Context,
    var list: MutableList<Hotspots>,
    val clickHotspotListAdapter: ClickHotspotListAdapter
) : RecyclerView.Adapter<ChangeCampAdapter.ViewHolder>() {


    var selected = -1

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val hotSpotNameText = itemView.hotspotNameAdapter
        val adapterHotspotOuterText = itemView.adapterHotspotOuter


        init {
            hotSpotNameText.setOnClickListener {
                selected = position
               clickHotspotListAdapter.selectedHotspot(list[position])
                notifyDataSetChanged()
            }
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_hotspot, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.hotSpotNameText.setText(list[position].hotspotSessionName)


        if (position == selected) {
            holder.adapterHotspotOuterText.setBackgroundColor(context.resources.getColor(R.color.colorPrimaryDark))
            holder.hotSpotNameText.setTextColor(context.resources.getColor(android.R.color.white))
        }else {
            holder.adapterHotspotOuterText.setBackgroundColor(context.resources.getColor(android.R.color.white))
            holder.hotSpotNameText.setTextColor(context.resources.getColor(android.R.color.black))
        }

    }

    interface ClickHotspotListAdapter {
        fun selectedHotspot(hotspots: Hotspots)
    }


    fun refreshList(){
        notifyDataSetChanged()
    }

}