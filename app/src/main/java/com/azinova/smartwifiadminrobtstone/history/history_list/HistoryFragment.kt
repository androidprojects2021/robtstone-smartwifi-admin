package com.azinova.smartwifiadminrobtstone.history.history_list

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.azinova.commons.Loader
import com.azinova.model.input.history.InputHistory
import com.azinova.model.input.resend_sms.InputResendSms
import com.azinova.model.response.history.DataItem
import com.azinova.smartwifiadminrobtstone.App
import com.azinova.smartwifiadminrobtstone.R
import com.azinova.smartwifiadminrobtstone.history.history_detail_popup.HistoryDetailPopup
import com.azinova.smartwifiadminrobtstone.login.LoginActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.history_fragment.*

class HistoryFragment : Fragment() ,HistoryAdapter.HistoryClick ,HistoryDetailPopup.HistoryDetailsClicks{

  private lateinit var viewModel: HistoryViewModel
  private lateinit var adapter: HistoryAdapter
  private lateinit var PHONE_NO: String



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.history_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(HistoryViewModel::class.java)

        PHONE_NO = requireArguments().getString("phone").toString()

        selectedPhone.text = PHONE_NO

        recyclerHistory.layoutManager=LinearLayoutManager(requireContext())
        adapter = HistoryAdapter(requireContext(),this)
        recyclerHistory.adapter=adapter

        callHistoryList(PHONE_NO)
        onClick()
        onObserve()
    }

    private fun onClick() {
        backHistoryList.setOnClickListener {
            activity?.onBackPressed()
        }



    }

    private fun onObserve() {


        viewModel.historyResponse.observe(viewLifecycleOwner, Observer {

            Loader.hideLoader()
            if(it.status.equals("success")) {
                viewModel.historyResponseClear()
                if(it.data?.size!=0){
                    recyclerHistory.visibility= View.VISIBLE
                    noDataHistory.visibility= View.GONE
                    adapter.refreshData(it.data)

                }else{
                    recyclerHistory.visibility= View.GONE
                    noDataHistory.visibility= View.VISIBLE
                }

            }else if(it.status.equals("token_error")){
                val intent = Intent(requireContext(), LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)

            }
            else if (! it.status.equals("idle")) {
                Snackbar.make(outerHistory, it.message.toString(), Snackbar.LENGTH_SHORT).show()
            }

        })


        viewModel.sendSmsResponse.observe(viewLifecycleOwner, Observer {

            Loader.hideLoader()
            if(it.status.equals("success")) {
                viewModel.sendSmsResponseClear()

                Toast.makeText(requireContext(),it.message,Toast.LENGTH_SHORT).show()

            }else if(it.status.equals("token_error")){
                val intent = Intent(requireContext(), LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)

            }
            else if (! it.status.equals("idle")) {
                Snackbar.make(outerHistory, it.message.toString(), Snackbar.LENGTH_SHORT).show()
            }

        })

    }

    private fun callHistoryList(phone:String) {
                Loader.showLoader(requireContext())
                viewModel.callHistory(InputHistory(App().sharefPrefs.user_details?.hotspotSessionId,
                App().sharefPrefs.user_details?.hotspotSessionName,
                App().sharefPrefs.user_details?.userId,
                phone,
                App().sharefPrefs.user_details?.token))
    }

    override fun clicked(dataItem: DataItem?) {
        val historyDetailPopup = HistoryDetailPopup(requireContext(),dataItem,PHONE_NO,this)
        historyDetailPopup.showPopup()
    }

    override fun sendMessage(dataItem: DataItem?) {
        Loader.showLoader(requireContext())
        viewModel.callResendSms(
            InputResendSms(App().sharefPrefs.user_details?.hotspotSessionId,
            dataItem?.voucherName,
            App().sharefPrefs.user_details?.hotspotSessionName,
            App().sharefPrefs.user_details?.userId,PHONE_NO,dataItem?.validity,App().sharefPrefs.user_details?.token))

    }

    override fun onPause() {
        super.onPause()
        try {
            Loader.hideLoader()
        } catch (e: Exception) {
        }
    }

}