package com.azinova.smartwifiadminrobtstone.sale

import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.azinova.commons.Loader
import com.azinova.model.input.last_voucher.InputLastVoucher
import com.azinova.model.input.saleVoucher.InputSaleVoucher
import com.azinova.model.input.validity.InputValidity
import com.azinova.model.response.validity.ProfilesItem
import com.azinova.smartwifiadminrobtstone.App
import com.azinova.smartwifiadminrobtstone.R
import com.azinova.smartwifiadminrobtstone.home.HomeActivity
import com.azinova.smartwifiadminrobtstone.login.LoginActivity
import com.azinova.smartwifiadminrobtstone.sale.sales_confirm.ConfirmPopupClick
import com.azinova.smartwifiadminrobtstone.sale.sales_confirm.SaleConfirmPopup
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.payment_fragment.*
import kotlinx.android.synthetic.main.sale_fragment.*

class SaleFragment : Fragment(),
    ConfirmPopupClick {



    private lateinit var viewModel: SaleViewModel
    private lateinit var parentActivity : HomeActivity

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.sale_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(SaleViewModel::class.java)
        parentActivity = activity as HomeActivity
        getSpinner()
        onClick()
        observe()
    }

    private fun getSpinner() {
        Loader.showLoader(requireContext())
        viewModel.validityApi(
            InputValidity(
                hotspotSessionId = App().sharefPrefs.user_details?.hotspotSessionId,
                hotspotSessionName = App().sharefPrefs.user_details?.hotspotSessionName,
                userId = App().sharefPrefs.user_details?.userId,
                token = App().sharefPrefs.user_details?.token
            )
        )
    }

    override fun onResume() {
        super.onResume()
        val locationManager = requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager

        if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            Toast.makeText(requireContext(), "Turn on GPS", Toast.LENGTH_SHORT).show()
            parentActivity.OnGPS()
        }
    }

    private fun observe() {

        viewModel.saleVoucherResponse.observe(viewLifecycleOwner, Observer {
            Loader.hideLoader()
            if(it.status.equals("success")){
                val bundle=Bundle()
                bundle.putString("status","success")
                bundle.putString("coupon_number",it.voucherName)
                bundle.putString("validity",it.validity)

                    findNavController().navigate(R.id.action_saleFragment_to_salesResponseFragment,bundle)
                    viewModel.clearResponse()

            }
            else if(it.status.equals("token_error")){
                val intent = Intent(requireContext(), LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)

            }
            else if (! it.status.equals("idle")) {
                Snackbar.make(outerSale, it.message.toString(), Snackbar.LENGTH_SHORT).show()
            }
        })


        viewModel.lastVoucherResponse.observe(viewLifecycleOwner, Observer {
            Loader.hideLoader()
            if(it.status.equals("success")){

                val validity : ProfilesItem? = validitySpinner.selectedItem as ProfilesItem?

                val saleConfirmPopup =
                    SaleConfirmPopup(
                        requireContext(), this, it.voucherDetail,
                        InputSaleVoucher(
                            hotspotSessionId = App().sharefPrefs.user_details?.hotspotSessionId,
                            hotspotSessionName = App().sharefPrefs.user_details?.hotspotSessionName,
                            mobile = phoneNumber.text.toString(),
                            validity = validity?.validityProfile,
                            userId = App().sharefPrefs.user_details?.userId,
                            token = App().sharefPrefs.user_details?.token
                        )
                    )
                saleConfirmPopup.showPopup()
                viewModel.clearLastResponse()
            }
            else if(it.status.equals("token_error")){
                val intent = Intent(requireContext(), LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }
            else if (! it.status.equals("idle")) {
                Snackbar.make(outerSale, it.message.toString(), Snackbar.LENGTH_SHORT).show()
            }
        })


        viewModel.validityResponse.observe(viewLifecycleOwner, Observer {
            Loader.hideLoader()
            if(it.status.equals("success")){

                if(it?.profiles?.size!! >0) {
                    val adapter = ValiditySpinnerAdapter(requireContext(),R.layout.custom_spinner, it.profiles ?: listOf() )
                    validitySpinner.adapter=adapter
                }else {
                    Snackbar.make(payOuter, "No validity available", Snackbar.LENGTH_SHORT).show()
                }
                viewModel.clearValidityResponse()
            }
            else if(it.status.equals("token_error")){
                val intent = Intent(requireContext(), LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }
            else if (! it.status.equals("idle")) {
                Snackbar.make(outerSale, it.message.toString(), Snackbar.LENGTH_SHORT).show()
            }
        })




    }


    private fun onClick() {

        submitButton.setOnClickListener {

           if(isValid()) {
               lastVoucherDetail()
           }
        }



    }

    private fun isValid() :Boolean{

        val validity : ProfilesItem? = validitySpinner.selectedItem as ProfilesItem?

        if(validity == null){
            Snackbar.make(payOuter, "No validity available", Snackbar.LENGTH_SHORT).show()
            return false
        }
       else if((phoneNumber.text.isNullOrBlank()||phoneNumber.text.toString().length!=10)){
            Snackbar.make(outerSale, "please enter valid phone number ", Snackbar.LENGTH_SHORT).show()
            return false
        }
        return true
    }

    private fun createVoucherApiCall(){
        Loader.showLoader(requireContext())
        val validity : ProfilesItem? = validitySpinner.selectedItem as ProfilesItem?
        viewModel.saleVoucher(
            InputSaleVoucher(
                hotspotSessionId = App().sharefPrefs.user_details?.hotspotSessionId,
                hotspotSessionName = App().sharefPrefs.user_details?.hotspotSessionName,
                mobile = phoneNumber.text.toString(),
                validity = validity?.validityProfile,
                userId = App().sharefPrefs.user_details?.userId,
                token = App().sharefPrefs.user_details?.token
            )
        )
    }

    private fun lastVoucherDetail(){
        Loader.showLoader(requireContext())
        viewModel.lastVoucher(InputLastVoucher(
            hotspotSessionId = App().sharefPrefs.user_details?.hotspotSessionId,
            hotspotSessionName = App().sharefPrefs.user_details?.hotspotSessionName,
            mobile = phoneNumber.text.toString(),
            userId = App().sharefPrefs.user_details?.userId,
            token = App().sharefPrefs.user_details?.token))
    }


    override fun clickOkButton() {

        createVoucherApiCall()
    }


    override fun onPause() {
        super.onPause()
        try {
            Loader.hideLoader()
        } catch (e: Exception) {
        }
    }

}