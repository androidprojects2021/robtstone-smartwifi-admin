package com.azinova.smartwifiadminrobtstone.sale

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.azinova.model.input.last_voucher.InputLastVoucher
import com.azinova.model.input.saleVoucher.InputSaleVoucher
import com.azinova.model.input.validity.InputValidity
import com.azinova.model.response.last_voucher.ResponseLastVoucher
import com.azinova.model.response.saleVoucher.ResponseSaleVoucher
import com.azinova.model.response.validity.ResponseValidity
import com.azinova.smartwifiadminrobtstone.App
import kotlinx.coroutines.launch

class SaleViewModel : ViewModel() {

    private val _saleVoucherResponse = MutableLiveData<ResponseSaleVoucher>()
    val saleVoucherResponse: LiveData<ResponseSaleVoucher>
        get() = _saleVoucherResponse

    private val _lastVoucherResponse = MutableLiveData<ResponseLastVoucher>()
    val lastVoucherResponse: LiveData<ResponseLastVoucher>
        get() = _lastVoucherResponse

    private val _validityResponse = MutableLiveData<ResponseValidity>()
    val validityResponse: LiveData<ResponseValidity>
        get() = _validityResponse



    fun clearResponse(){
        _saleVoucherResponse.value = ResponseSaleVoucher( status = "idle")
    }

    fun clearLastResponse(){
        _lastVoucherResponse.value = ResponseLastVoucher( status = "idle")
    }

    fun clearValidityResponse(){
        _validityResponse.value = ResponseValidity(status = "idle")
    }

    fun validityApi(inputValidity: InputValidity){

        viewModelScope.launch {
            try {

                val response = App().retrofit.getValidity(inputValidity)
                _validityResponse.value =response

            } catch (e: Exception) {

                _validityResponse.value = ResponseValidity(message = "Something went wrong" ,status = "Error")
            }
        }

    }


    fun saleVoucher(inputsaleVoucher: InputSaleVoucher) {
        viewModelScope.launch {
            try {

                val response = App().retrofit.createSale(inputsaleVoucher)
                _saleVoucherResponse.value = response

            } catch (e: Exception) {

                _saleVoucherResponse.value = ResponseSaleVoucher(message = "Something went wrong" ,status = "Error")
            }
        }

    }

    fun lastVoucher(inputLastVoucher: InputLastVoucher) {
        viewModelScope.launch {
            try {

                val response = App().retrofit.lastVoucher(inputLastVoucher)
                _lastVoucherResponse.value = response

            } catch (e: Exception) {

                _lastVoucherResponse.value = ResponseLastVoucher(message = "Something went wrong" ,status = "Error")
            }
        }

    }


}