package com.azinova.smartwifiadminrobtstone.sale.sales_confirm

interface ConfirmPopupClick {
    fun clickOkButton()
}