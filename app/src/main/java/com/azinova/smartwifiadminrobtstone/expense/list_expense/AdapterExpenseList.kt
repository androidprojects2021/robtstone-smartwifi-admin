package com.azinova.smartwifiadminrobtstone.expense.list_expense

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.azinova.model.response.expense_list.PaymentsItem
import com.azinova.model.response.report.CouponsItem
import com.azinova.smartwifiadminrobtstone.R
import kotlinx.android.synthetic.main.adapter_expense_list.view.*

class AdapterExpenseList (val context: Context): RecyclerView.Adapter<AdapterExpenseList.ViewHolder>() {

    var data : List<PaymentsItem?>? = listOf()

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        val addedDate : TextView = itemView.addedDate
        val expenseAmount : TextView = itemView.expAmount
        val expenseCategory : TextView = itemView.category
        val expenseDate : TextView = itemView.expenseDate
        val supplierName : TextView = itemView.supplier
        val expenseId : TextView  = itemView.idExp
        val description : TextView  = itemView.descriptionTest


    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterExpenseList.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_expense_list,parent,false))
    }

    override fun getItemCount(): Int {
        return data?.size ?:0
    }

    override fun onBindViewHolder(holder: AdapterExpenseList.ViewHolder, position: Int) {

       holder.addedDate.text = "Added On : ${ data?.get(position)?.addeddate ?:"" }"
       holder.expenseAmount.text = "AED ${data?.get(position)?.expenseAmount}"
       holder.expenseCategory.text = "Category : ${data?.get(position)?.expenseCategory}"
       holder.expenseDate.text = "Expense Date : ${ data?.get(position)?.expenseDate}"
       holder.supplierName.text = "Supplier : ${data?.get(position)?.supplierName}"
       holder.expenseId.text = "# ID : ${data?.get(position)?.expenseId}"
       holder.description.text =  data?.get(position)?.description ?:""


    }


    fun refreshData(data : List<PaymentsItem?>?){
        this.data = data
        notifyDataSetChanged()
    }


    interface ClickReport{
        fun reportSelected(item : CouponsItem)
    }

}