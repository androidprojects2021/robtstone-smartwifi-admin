package com.azinova.smartwifiadminrobtstone.payments


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.azinova.model.response.users.UsersItem
import com.azinova.smartwifiadminrobtstone.R


class UserSpinnerAdapter( context: Context,val layout:Int,val data:List<UsersItem?>): ArrayAdapter<UsersItem>(context,layout,data) {


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        var currentItemView = convertView
        if (currentItemView == null) {
            currentItemView = LayoutInflater.from(context).inflate(R.layout.custom_spinner, parent, false)
        }

        val textView1 = currentItemView!!.findViewById<TextView>(R.id.textSpin)
        textView1.text = data[position]?.name

        return currentItemView

    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return createViewFromResource(position, convertView, parent)
    }

    private fun createViewFromResource(position: Int, convertView: View?, parent: ViewGroup?): View{
        val view: TextView = convertView as TextView? ?: LayoutInflater.from(context).inflate(layout, parent, false) as TextView
        view.text = data[position]?.name
        return view
    }



}