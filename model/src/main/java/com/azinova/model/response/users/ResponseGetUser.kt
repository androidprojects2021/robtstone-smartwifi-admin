package com.azinova.model.response.users

import com.google.gson.annotations.SerializedName

data class ResponseGetUser(

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("users")
	val users: List<UsersItem?>? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class UsersItem(

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("name")
	val name: String? = null
)
