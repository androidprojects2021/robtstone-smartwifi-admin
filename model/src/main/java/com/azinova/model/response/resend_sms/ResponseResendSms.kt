package com.azinova.model.response.resend_sms

import com.google.gson.annotations.SerializedName

data class ResponseResendSms(

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)
