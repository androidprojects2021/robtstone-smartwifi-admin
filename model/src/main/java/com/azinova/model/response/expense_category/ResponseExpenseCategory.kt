package com.azinova.model.response.expense_category

import com.google.gson.annotations.SerializedName

data class ResponseExpenseCategory(

	@field:SerializedName("data")
	val data: List<DataItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class DataItem(

	@field:SerializedName("category_name")
	val categoryName: String? = null,

	@field:SerializedName("id")
	val id: String? = null
)
