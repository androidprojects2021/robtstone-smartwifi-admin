package com.azinova.model.response.expense_add

import com.google.gson.annotations.SerializedName

data class ResponseExpenseAdd(

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)
